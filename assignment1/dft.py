import cmath
import numpy as np
import time
import search

def dft(x,N):
    W_N = cmath.exp(-2*cmath.pi*1J/N)
    X = []
    for k in range(N):
        tmp = 0
        for n in range(N):
            tmp += x[n]*W_N**(n*k)
        X.append(tmp)
    return X

if __name__ == '__main__':
    Fs = 400
    f1, f2 = 50, 65
    N_range = [2,4,8,16,32,64,128, 256, 1024, 2048]
    time_taken = []
    for N in N_range:
      k = [i*2*cmath.pi*f1*1.0/Fs for i in range(N)]
      signal_1 = np.sin(k)
      signal_2 = np.sin(k)
      st_time = time.time()
      out1 = dft(signal_1,N)
      time_taken.append(time.time() - st_time)
      out2 = dft(signal_2,N)
    print time_taken
    search.graphify(N_range, time_taken, 'Range of N', 'Time taken', 'DFT-Time',\
            'dft_exec')
