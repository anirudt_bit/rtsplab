import time, os
import timeit
import cProfile
import numpy as np
from random import randint
import matplotlib.pyplot as plt

# k = [5,4,3,2,1]

arr = []
for i in range(10):
    arr.append(randint(0,100))

print arr
req = arr[5]

def lin_search(k, required):
    idx = 0
    for i in k:
        if i==required:
            return idx
        idx+=1
    return -1

def select_sort(k):
    n = len(k)
    for i in range(0,n-1):
        min_idx = i
        for j in range(i+1,n):
            if k[j] < k[min_idx]:
                min_idx = j
        k[i], k[min_idx] = k[min_idx], k[i]
    return k


def bubble_sort(k):
    n = len(k)
    for i in range(0,n-1):
        for j in range(0,n-1):
            if k[j]>k[j+1]:
                k[j+1],k[j] = k[j], k[j+1]
    return k

def worst_time_profiler():
    lin, sel, bub = [], [], []
    length = 2000
    for i in range(10,length,10):
        st_tm = time.time()
        k = np.random.randint(100, size=i)
        out = lin_search(k, 102)
        del_time = time.time() - st_tm
        lin.append(del_time)
    print "Linear done"

    for i in range(10,length,10):
        st_tm = time.time()
        k = range(i)
        k = k[::-1]
        out = select_sort(k)
        del_time = time.time() - st_tm
        sel.append(del_time)
    print "Selection Done"

    timer = []
    for i in range(10,length,10):
        st_tm = time.time()
        k = range(i)
        k = k[::-1]
        out =bubble_sort(k)
        del_time = time.time() - st_tm
        timer.append(i)
        bub.append(del_time)
    print "Bubble done"

    graphify(timer, lin, 'Length', 'Time', 'Linear Search-Worst Case Execution',\
            'linear')
    graphify(timer, sel, 'Length', 'Time', 'Selection Sort-Worst Case Execution',\
            'select')
    graphify(timer, bub, 'Length', 'Time', 'Bubble Sort-Worst Case Execution',\
            'bubble')

def avg_profiling():
    global arr, req
    lin, sel, bub = [], [], []
    arr = []
    length = 1000
    setup = "import search"
    epochs = 5
    del_time = []
    timer = []
    for i in range(10,length,10):
        for j in range(1,epochs):
            st_tm = time.time()
            arr = np.random.randint(100, size=i)
            req = arr[len(arr)/2]
            out = lin_search(arr,102)
            del_time.append(time.time()-st_tm)
        lin.append(np.mean(del_time))
        del_time = []
        timer.append(i)
    print "Linear done"
    graphify(timer, lin, 'Length', 'Time', 'Linear Sort-Average Case Execution',\
            'linear_avg')

    for i in range(10,length,10):
        for j in range(1,epochs):
            st_tm = time.time()
            arr = np.random.randint(100, size=i)
            out = select_sort(arr)
            del_time.append(time.time()-st_tm)
        sel.append(np.mean(del_time))
        del_time = []
    print "Selection done"

    timer = []
    for i in range(10,length,10):
        for j in range(1,epochs):
            st_tm = time.time()
            arr = np.random.randint(100, size=i)
            out = bubble_sort(arr)
            del_time.append(time.time()-st_tm)
        bub.append(np.mean(del_time))
        timer.append(i)
        del_time = []
    print "Bubble sort done"
    graphify(timer, sel, 'Length', 'Time', 'Selection Sort-Average Case Execution',\
            'select_avg')
    graphify(timer, bub, 'Length', 'Time', 'Bubble Sort-Average Case Execution',\
            'bubble_avg')

def graphify(x, y, xlabel, ylabel, title, name):
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title(title)
    plt.plot(x, y)
    plt.grid = True
    plt.savefig(name+'.png')
    plt.show()

if __name__ == '__main__':
    # Computing time complexity of linear search
    # setup = "from __main__ import lin_search"
    # print timeit.timeit("lin_search()", setup=setup,number=1)

    # Linear Search Space complexity
    # cProfile.run('lin_search()')

    # print select_sort()
    # Selection Sort: Time complexity
    #setup = "from __main__ import select_sort"
    # print timeit.timeit("select_sort()", setup=setup, number=1)

    # Selection Sort: Space complexity
    # cProfile.run('select_sort()')

    # Bubble Sort: Time complexity
    # setup = "from __main__ import bubble_sort"
    # print timeit.timeit("bubble_sort()",setup=setup, number=1)

    # Bubble Sort: Space complexity
    # cProfile.run("bubble_sort()")
    # print bubble_sort()
    # time_profiler()

    worst_time_profiler()
    avg_profiling()
