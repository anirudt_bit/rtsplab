#!/bin/bash

import cmath
import numpy as np
import time
import matplotlib.pyplot as plt
from scipy import signal
from scipy.fftpack import *
import pdb

def dft(x):
    N = len(x)
    W_N = cmath.exp(-2J*cmath.pi*1.0/N)
    W_N_mat = []
    for i in range(N):
        tmp = [W_N**(i*j) for j in range(N)]
        W_N_mat.append(tmp)
    W_N_mat = np.array(W_N_mat)
    print W_N_mat
    x = np.array(x)
    X = np.dot(W_N_mat, x)
    return X

def idft(X): 
    N = len(X)
    W_N = cmath.exp(-2J*cmath.pi*1.0/N)
    W_N_mat = []
    for i in range(N):
        tmp = [W_N**(i*j) for j in range(N)]
        W_N_mat.append(tmp)
    W_N_mat = np.array(W_N_mat)
    X = np.array(X)
    x = np.dot(np.linalg.inv(W_N_mat),X)
    return x

def graphify(x, y, xlabel, ylabel, title, name):
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title(title)
    plt.stem(y,x)
    plt.grid = True
    plt.savefig(name+'.png')
    plt.show()

def graphify_plot(x, y, xlabel, ylabel, title, name):
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title(title)
    plt.plot(x, y)
    plt.grid = True
    plt.savefig(name+'.png')
    plt.show()

def circular_conv_dft(x_1, x_2):
    X_1 = dft(x_1)
    X_2 = dft(x_2)
    X_circ = np.multiply(X_1, X_2)
    x_circ= idft(X_circ)
    return x_circ

def linear_conv(x_1, x_2):
    i = 0
    x_out = []
    while i<len(x_1)+len(x_2)-1:
        tmp = 0
        j = 0
        while j<len(x_2) and i>=j:
            if i-j>=len(x_1):
                j+=1
                continue
            tmp += x_1[i-j]*x_2[j]
            j+=1
        i+=1
        x_out.append(tmp)
    return np.convolve(x_1, x_2)

def circular_conv_linear(x_1, x_2):
    x_out = linear_conv(x_1, x_2)
    y_out = x_out[0:len(x_2)]
    residual = x_out[len(x_2):]
    print x_out
    print y_out
    i = 0
    while i < len(residual):
        y_out[i] += residual[i]
        i+=1
    print y_out
    return y_out



def test_dft_idft():
    # Do something here
    N = 200
    # Generate a x[n] signal of length N

    Fs = 200
    f1, f2 = 50, 65
    N_range = [128, 256, 1024]
    k = [i*2*cmath.pi*f1*1.0/Fs for i in range(N)]
    signal_1 = np.sin(k)

    # and a X[k] signal of length N to test idft
    st_time = time.time() 
    X = dft(signal_1)
    pdb.set_trace()
    time_taken = time.time() - st_time
    mag_X = [abs(i) for i in X]
    print mag_X
    graphify(mag_X, range(N), 'N points', 'Magnitude',\
            'N point DFT for 50Hz wave', 'dft')
    print "Time taken is ", time_taken

def test_side_band():
    Fs = 600
    N = 600
    f = 5
    k = [i*2*cmath.pi*f*1.0/Fs for i in range(N)]
    x = signal.square(k)
    graphify_plot(range(N), x, 'time', 'magnitude', 'square wave signal',\
            'squaretime')
    y = fft(x)
    pos_y = y[0:N/2]*2.0/N
    print pos_y
    y_spacing = np.linspace(0.0, Fs*1.0/(2.0), N/2)
    graphify(y_spacing, abs(pos_y), 'Frequency Domain', 'Magnitude',\
                'Square Wave Signal', 'fftsquare')
    for i in range(2,5):
        rect_filter = [1 for j in range(N/(i*2))] + [0 for j in range(N/(i*2))]
        conv_spacing = np.linspace(0.0, Fs*1.0/(2.0), N/(1.0*i))
        convolved_result = [a*b for a,b in zip(pos_y, rect_filter)]
        print len(conv_spacing), len(convolved_result)
        graphify(conv_spacing, np.abs(convolved_result), 'Frequency Domain',\
                'Magnitude','Convolved Signal', 'fftconv'+str(i))
        x_dash = ifft(convolved_result)
        print len(x_dash)
        graphify_plot(range(len(x_dash)), x_dash, 'time', 'magnitude',\
                'square wave signal', 'squaretime'+str(i))


def test_circ_dft():
    x_1 = [2,1,2,1]
    x_2 = [1,2,3,4]
    circ = circular_conv_dft(x_1, x_2)
    print circ

if __name__ == '__main__':
    test_dft_idft()
    # test_circ_dft()
    # linear_conv([1,2,3], [4,5,6])
    # circular_conv_linear([1,2,3], [4,5,6])
    # test_side_band()
