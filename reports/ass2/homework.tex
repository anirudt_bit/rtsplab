\documentclass{article}

\usepackage{fancyhdr}
\usepackage{extramarks}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{amsfonts}
\usepackage{tikz}
\usepackage{listings}
\usepackage[plain]{algorithm}
\usepackage{algpseudocode}
\usepackage{graphicx}
\usepackage[hidelinks]{hyperref}
\usepackage{xcolor}

% Basic Document Settings
%

\topmargin=-0.45in
\evensidemargin=0in
\oddsidemargin=0in
\textwidth=6.5in
\textheight=9.0in
\headsep=0.25in

\linespread{1.1}

\pagestyle{fancy}
\lhead{\hmwkAuthorName}
\chead{\hmwkClass:\ \hmwkTitle}
\rhead{\firstxmark}
\lfoot{\lastxmark}
\cfoot{\thepage}

\renewcommand\headrulewidth{0.4pt}
\renewcommand\footrulewidth{0.4pt}

\setlength\parindent{0pt}

%
% Create Problem Sections
%

%\newcommand{\enterProblemHeader}[1]{
%    \nobreak\extramarks{}{Problem \arabic{#1} continued on next page\ldots}\nobreak{}
%    \nobreak\extramarks{Problem \arabic{#1} (continued)}{Problem \arabic{#1} continued on next page\ldots}\nobreak{}
%}

%\newcommand{\exitProblemHeader}[1]{
%    \nobreak\extramarks{Problem \arabic{#1} (continued)}{Problem \arabic{#1} continued on next page\ldots}\nobreak{}
%    \stepcounter{#1}
%    \nobreak\extramarks{Problem \arabic{#1}}{}\nobreak{}
%}

\setcounter{secnumdepth}{0}
\newcounter{partCounter}

%
% Homework Problem Environment
%
% This environment takes an optional argument. When given, it will adjust the
% problem counter. This is useful for when the problems given for your
% assignment aren't sequential. See the last 3 problems of this template for an
% example.
%

%
% Homework Details
%   - Title
%   - Due date
%   - Class
%   - Section/Time
%   - Instructor
%   - Author
%

\newcommand{\hmwkTitle}{Assignment\ \#2}
\newcommand{\hmwkDueDate}{Spring 2016}
\newcommand{\hmwkClass}{Real Time Signal Processing Lab}
\newcommand{\hmwkClassInstructor}{Professor Debdoot Sheet}
\newcommand{\hmwkAuthorName}{Anirud Thyagharajan}
\newcommand{\hmwkRollNo}{12EE35011}
\newcommand{\matr}[1]{\mathbf{#1}}
\graphicspath{{images/}}

%
% Title Page
%

\title{
    \vspace{2in}
    \textmd{\textbf{\hmwkClass:\ \hmwkTitle}}\\
    \normalsize\vspace{0.1in}\small{\hmwkDueDate}\\
    \vspace{0.1in}\large{\textit{\hmwkClassInstructor}}
    \vspace{3in}
}

\author{\textbf{\hmwkAuthorName}\\ \textbf{\hmwkRollNo}}
\date{}

\renewcommand{\part}[1]{\textbf{\large Part \Alph{partCounter}}\stepcounter{partCounter}\\}

%
% Various Helper Commands
%

% Useful for algorithms
\newcommand{\alg}[1]{\textsc{\bfseries \footnotesize #1}}

% For derivatives
\newcommand{\deriv}[1]{\frac{\mathrm{d}}{\mathrm{d}x} (#1)}

% For partial derivatives
\newcommand{\pderiv}[2]{\frac{\partial}{\partial #1} (#2)}

% Integral dx
\newcommand{\dx}{\mathrm{d}x}

% Alias for the Solution section header
\newcommand{\solution}{\textbf{\large Solution}}

% Probability commands: Expectation, Variance, Covariance, Bias
\newcommand{\E}{\mathrm{E}}
\newcommand{\Var}{\mathrm{Var}}
\newcommand{\Cov}{\mathrm{Cov}}
\newcommand{\Bias}{\mathrm{Bias}}

\begin{document}

\maketitle

\pagebreak

  \lstset{language=Python,
          basicstyle=\ttfamily,
          keywordstyle=\color{blue}\ttfamily,
          stringstyle=\color{red}\ttfamily,
          commentstyle=\color{green}\ttfamily,
}
\section{Discrete Fourier Transforms and Implementations}
\par\null\par
    The main objectives of this assignment are:
    \begin{itemize}
        \item DFT and IDFT using vector matrix multiplication.
        \item Reduction of sidebands using windowing technique.
        \item Circular convolution using linear convolution, and circular convolution using DFT.
    \end{itemize}
    We will deal with each of the above processes, discuss their pseudo code, and present the results of the algorithms. 
    \par\null\par
    The Python files are appended at the end. Apart from this, the respective implementations can be found with their algorithms.
    \par\null\par
    I also use \textbf{Git} for version control, so you could find my repository at \textcolor{blue}{\underline{\href{https://bitbucket.org/anirudt_bit/rtsplab/src}{this link}}}.

\pagebreak

\section{DFT \& IDFT using Vectorization}
\par\null\par
The DFT can be vectorized in the following form:
\[
  \matr{x_N} = [x[0]\ x[1] \dots \ x[N-1]]^T,
  \\
  \matr{X_N} = [X[0]\ X[1] \dots \ X[N-1]]^T
\] and taking:\\
\[W_N = \exp(-\frac{j2\pi}{N}),\] and thus\\
\[\matr{W_N} = 
        \begin{bmatrix}
              1 & 1 & \dots & 1\\
              1 & W_N &\dots & {W_N}^{N-1}\\
          \dots & \dots & \dots & \dots\\
              1 & {W_N}^{N-1} & \dots & {W_N}^{\left( N-1 \right)\left( N-1 \right)}\\
        \end{bmatrix}
      \]  
        \\
        Thus, the DFT and IDFT equations will form as follows:
        \[
          \matr{X_N} = \matr{W_N}\matr{x_N},
          \\
          \matr{x_N} = \matr{W_N^{-1}}\matr{X_N}
        \]
    Thus, the following implementation is in Python.
\lstinputlisting[firstline=10, lastline=33]{dft.py}
        \pagebreak
The following graph depicts the DFT of a sine wave of frequency 50 Hz.

      \parbox{\linewidth} {
        \centering
        \includegraphics[scale=0.5]{dft}
      }
      \pagebreak
\section{Reduction of sidebands using windowing methods}
\par\null\par
In this section, we proceed to study low pass filtering, by attempting the following: The higher harmonics of the frequency response of a square wave would be eliminated using a rectangular window, and the resulting time domain signal would be reconstructed. We employ the Fast Fourier Transform (FFT) to transform the signal from the time domain to the frequency domain, and the IFFT for the return route.

\lstinputlisting[firstline=111, lastline=137]{dft.py}
\pagebreak
Now, we will go over the plots one by one. \\
Firstly, this is the square wave in time domain.
\\
      \parbox{\linewidth} {
        \centering
        \includegraphics[scale=0.5]{squaretime}
      }
      \\
      \parbox{\linewidth} {
        \centering
        \includegraphics[scale=0.5]{fftsquare}
      }
      \parbox{\linewidth} {
        \centering
        \includegraphics[scale=0.4]{fftconv2}
      }
      \parbox{\linewidth} {
        \centering
        \includegraphics[scale=0.4]{squaretime2}
      }
      \parbox{\linewidth} {
        \centering
        \includegraphics[scale=0.4]{fftconv3}
      }
      \parbox{\linewidth} {
        \centering
        \includegraphics[scale=0.4]{squaretime3}
      }
      \parbox{\linewidth} {
        \centering
        \includegraphics[scale=0.4]{fftconv4}
      }
      \parbox{\linewidth} {
        \centering
        \includegraphics[scale=0.4]{squaretime4}
      }

Thus, we can notice that as our rectangular window shrinks, the allowable frequency range reduces, and this decreases the number of harmonics that are accounted for, thus distorting the reconstructed signal.
\par\null\par
Thus, for a given amount of \textit{Total Harmonic Distortion} in the problem, the bandwidth of the window can be designed, thus giving the clarity of the reconstructed signal.
\pagebreak
\section{Circular Convolution}
In the assignment, the circular convolution of two signals is evaluated by two methods:
\\
\begin{itemize}
  \item From the linear convolution of the two signals
  \item From the multiplication of DFTs of the two signals
\end{itemize}
\par\null\par
\subsection{DFT}
It is a known corrollary that the multiplication of the DFTs of two signals evaluates to the circular convolutions of the two signals in the time domain. Utilizing this fact, and the implemented functions hitherto for DFT and inverse DFT,\\
\par\null\par
\lstinputlisting[firstline=53, lastline=59]{dft.py}

\subsection{Linear Convolution}
Circular convolution can be conceptualized as linear convolution summed over a finite group, i.e. the circular convolution is the sum of the linear convolution elements over the modulo of the number of elements in the signal vector. Programmatically, the implementation is as follows:\\
\par\null\par
\lstinputlisting[firstline=76, lastline=88]{dft.py}
\pagebreak
Also, the implementation of linear convolution is:\\
\lstinputlisting[firstline=60, lastline=75]{dft.py}
\pagebreak

\section{Appendix: Master code for implementing the assignment}
\lstinputlisting{dft.py}
\pagebreak

\end{document}
