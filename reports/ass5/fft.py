import numpy as np
import matplotlib.pyplot as plt
import time, os, sys
import pdb
import cmath

def dft(x):
    N = len(x)
    W_N = cmath.exp(-2J*cmath.pi*1.0/N)
    W_N_mat = []
    for i in range(N):
        tmp = [W_N**(i*j) for j in range(N)]
        W_N_mat.append(tmp)
    W_N_mat = np.array(W_N_mat)
    x = np.array(x)
    X = np.dot(W_N_mat, x)
    return X

def idft(X): 
    N = len(X)
    W_N = cmath.exp(-2J*cmath.pi*1.0/N)
    W_N_mat = []
    for i in range(N):
        tmp = [W_N**(i*j) for j in range(N)]
        W_N_mat.append(tmp)
    W_N_mat = np.array(W_N_mat)
    X = np.array(X)
    x = np.dot(np.linalg.inv(W_N_mat),X)
    return x

def dit_fft(x):
    x_odd = [x[i] for i in range(len(x)) if i%2==1 ]
    x_even = [x[i] for i in range(len(x)) if i%2==0 ]
    X_odd = dft(x_odd)
    X_even = dft(x_even)
    N = len(x)
    W_N = cmath.exp(2J*cmath.pi*1.0/N)
    tmp = 0
    X = []
    for i in range(len(x)):
        X.append(X_even[i%(N/2)] + W_N**(-i)*X_odd[i%(N/2)])
    return X

def dif_fft(x):
    N = len(x)
    even_sig = [x[i] + x[i+N//2] for i in range(N//2)]
    W_N = cmath.exp(2J*cmath.pi*1.0/N)
    odd_sig = [(x[i] - x[i+N//2])*W_N**(-i) for i in range(N//2)]
    X_even = dft(even_sig)
    X_odd = dft(odd_sig)
    #print X_even, X_odd
    X = [X_even[i//2] if i%2==0 else X_odd[i//2] for i in range(N)]
    return X

def dif_fft_recursive(N, x):
    # Base case
    if N==1:
        return x
    else:
        N_ = N//2
        xe, xo = [], []
        W_N = cmath.exp(2J*cmath.pi*1.0/N)
        for i in range(N_):
            xe.append(x[i]+x[i+N//2])
            xo.append((x[i]-x[i+N//2])*W_N**(-i))

        # Recursive step
        Xe = dif_fft_recursive(N_, xe)
        Xo = dif_fft_recursive(N_, xo)

        # Assimilation
        X = [Xe[i//2] if i%2==0 else Xo[i//2] for i in range(N)]
        return X

def dit_fft_recursive(N, x):
    # Base case
    if N==1:
        return x
    else:
        N_ = N//2
        xe = [x[2*i] for i in range(N_)]
        xo = [x[2*i+1] for i in range(N_)]
        Xe = dit_fft_recursive(N_, xe)
        Xo = dit_fft_recursive(N_, xo)
        W_N = cmath.exp(2J*cmath.pi*1.0/N)
        X = range(N)
        for i in range(N_):
            X[i] = Xe[i]+Xo[i]*W_N**(-i)
            X[i+N_] = Xe[i] - Xo[i]*W_N**(-i)

        return X

def graphify(x, y, xlabel, ylabel, title, name):
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title(title)
    plt.plot(x, y)
    plt.grid = True
    plt.savefig(name+'.png')

def test_complexity():
    num_test = 10
    sizes = [2**i for i in range(1,num_test+1)]
    time_dft, time_dit, time_dif = [],  [], []
    # Testing DFT, DIT-FFT and DIT_FFT
    for i in range(num_test):
        x = range(sizes[i])
        start_time = time.time()
        tmp = dft(x)
        time_dft.append(time.time()-start_time)
        
        start_time = time.time()
        tmp = dit_fft_recursive(len(x), x)
        time_dit.append(time.time() - start_time)

        start_time = time.time()
        tmp = dif_fft_recursive(len(x), x)
        time_dif.append(time.time() - start_time)

    graphify(sizes, time_dft, "Length", "Time", "DFT Time",\
            "dft_time")
    graphify(sizes, time_dit, "Length", "Time", "DIT Time",\
            "dit_time")
    graphify(sizes, time_dif, "Length", "Time", "DIF Time",\
            "dif_time")

    plt.show()

if __name__ == "__main__":
    # test_fft()
    # test_ifft()

    #x = [82, 44, 62, 79, 92, 74, 18, 41]
    #X = dit_fft(x)

    #X = dif_fft(x)
    #print X
    test_complexity()

    #print dit_fft_recursive(4, [1,2,3,4])
