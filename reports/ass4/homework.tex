\documentclass{article}

\usepackage{fancyhdr}
\usepackage{extramarks}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{amsfonts}
\usepackage{tikz}
\usepackage{listings}
\usepackage[plain]{algorithm}
\usepackage{algpseudocode}
\usepackage{graphicx}
\usepackage[hidelinks]{hyperref}
\usepackage{xcolor}

% Basic Document Settings
%

\topmargin=-0.45in
\evensidemargin=0in
\oddsidemargin=0in
\textwidth=6.5in
\textheight=9.0in
\headsep=0.25in

\linespread{1.1}

\pagestyle{fancy}
\lhead{\hmwkAuthorName}
\chead{\hmwkClass:\ \hmwkTitle}
\rhead{\firstxmark}
\lfoot{\lastxmark}
\cfoot{\thepage}

\renewcommand\headrulewidth{0.4pt}
\renewcommand\footrulewidth{0.4pt}

\setlength\parindent{0pt}

%
% Create Problem Sections
%

%\newcommand{\enterProblemHeader}[1]{
%    \nobreak\extramarks{}{Problem \arabic{#1} continued on next page\ldots}\nobreak{}
%    \nobreak\extramarks{Problem \arabic{#1} (continued)}{Problem \arabic{#1} continued on next page\ldots}\nobreak{}
%}

%\newcommand{\exitProblemHeader}[1]{
%    \nobreak\extramarks{Problem \arabic{#1} (continued)}{Problem \arabic{#1} continued on next page\ldots}\nobreak{}
%    \stepcounter{#1}
%    \nobreak\extramarks{Problem \arabic{#1}}{}\nobreak{}
%}

\setcounter{secnumdepth}{0}
\newcounter{partCounter}

%
% Homework Problem Environment
%
% This environment takes an optional argument. When given, it will adjust the
% problem counter. This is useful for when the problems given for your
% assignment aren't sequential. See the last 3 problems of this template for an
% example.
%

%
% Homework Details
%   - Title
%   - Due date
%   - Class
%   - Section/Time
%   - Instructor
%   - Author
%

\newcommand{\hmwkTitle}{Assignment\ \#4}
\newcommand{\hmwkDueDate}{Spring 2016}
\newcommand{\hmwkClass}{Real Time Signal Processing Lab}
\newcommand{\hmwkClassInstructor}{Professor Debdoot Sheet}
\newcommand{\hmwkAuthorName}{Anirud Thyagharajan}
\newcommand{\hmwkRollNo}{12EE35011}
\newcommand{\matr}[1]{\mathbf{#1}}
\graphicspath{{images/}}

%
% Title Page
%

\title{
    \vspace{2in}
    \textmd{\textbf{\hmwkClass:\ \hmwkTitle}}\\
    \normalsize\vspace{0.1in}\small{\hmwkDueDate}\\
    \vspace{0.1in}\large{\textit{\hmwkClassInstructor}}
    \vspace{3in}
}

\author{\textbf{\hmwkAuthorName}\\ \textbf{\hmwkRollNo}}
\date{}

\renewcommand{\part}[1]{\textbf{\large Part \Alph{partCounter}}\stepcounter{partCounter}\\}

%
% Various Helper Commands
%

% Useful for algorithms
\newcommand{\alg}[1]{\textsc{\bfseries \footnotesize #1}}

% For derivatives
\newcommand{\deriv}[1]{\frac{\mathrm{d}}{\mathrm{d}x} (#1)}

% For partial derivatives
\newcommand{\pderiv}[2]{\frac{\partial}{\partial #1} (#2)}

% Integral dx
\newcommand{\dx}{\mathrm{d}x}

% Alias for the Solution section header
\newcommand{\solution}{\textbf{\large Solution}}

% Probability commands: Expectation, Variance, Covariance, Bias
\newcommand{\E}{\mathrm{E}}
\newcommand{\Var}{\mathrm{Var}}
\newcommand{\Cov}{\mathrm{Cov}}
\newcommand{\Bias}{\mathrm{Bias}}

\begin{document}

\maketitle

\pagebreak

  \lstset{language=Python,
          basicstyle=\ttfamily,
          keywordstyle=\color{blue}\ttfamily,
          stringstyle=\color{red}\ttfamily,
          commentstyle=\color{green}\ttfamily,
}
\section{Filter Design Problem}
\par\null\par
  The objective of the assignment is to design a filter which will denoise a given noisy signal.
  Thus, we apply filters to separate frequency components and analyse the resulting time domain signal.
    We will discuss an example and implement the algorithm in Python.
    \par\null\par
    The Python files are appended at the end. 
    \par\null\par
    I also use \textbf{Git} for version control, so you could find my repository at \textcolor{blue}{\underline{\href{https://bitbucket.org/anirudt_bit/rtsplab/src}{this link}}}.

\pagebreak

\section{Filter Design}
\par\null\par
We employ the \textit{Frequency Sampling method}, which is outlined in the next section. To 
analyse the frequency components of the given noisy data, we use the \textit{FFT} of the signal, accompanied
by a shifting of the FFT to make it zero centred.
\\
We observe the following as the time domain signal, and below it, the frequency domain signal at first glance.
      \parbox{\linewidth} {
        \centering
        \includegraphics[scale=0.4]{data_timefull}
        %\captionof{figure}{Worst Case complexity}
      }
\null\par\null
      \parbox{\linewidth} {
        \centering
        \includegraphics[scale=0.4]{data_freqfull}
        %\captionof{figure}{Worst Case complexity}
      }
\null\par\null
      \parbox{\linewidth} {
        \centering
        \includegraphics[scale=0.4]{psd-freqfullideal_data_out}
        %\captionof{figure}{Worst Case complexity}
      }

      If we zoom in to see the actual frequency components, we see:\\
      \parbox{\linewidth} {
        \centering
        \includegraphics[scale=0.4]{full_ideal_fft}
        %\captionof{figure}{Worst Case complexity}
      }
       Thus, we will make two kinds of filters, and try removing/passing certain frequency components
       and analyse the sense of the resulting time domain signal.
\\

       Making a filter, of lower order, having a pass band bandwidth of 30 points, and single sided transition width of 15 points, we obtain the resulting time domain signal as:\\
      \parbox{\linewidth} {
        \centering
        \includegraphics[scale=0.4]{fullless_ideal_data_out}
        %\captionof{figure}{Worst Case complexity}
      }
    As we increase the order of the filter, or equivalently, we reduce the transition width to one point [thereby
    making it more or less ideal], we get a curve with better rejection:\\
      \parbox{\linewidth} {
        \centering
        \includegraphics[scale=0.4]{fullideal_data_out}
        %\captionof{figure}{Worst Case complexity}
      }
      Thus, it has better rejection, and the sine wave is much more smoother in this case.

      \subsection{Choosing half the points}
      Now, we go on to truncate the original signal, thus giving only 10000 points of time domain
      data, and we proceed as done before. Thus, \\
We observe the following as the time domain signal, and below it, the frequency domain signal at first glance.
      \parbox{\linewidth} {
        \centering
        \includegraphics[scale=0.4]{data_timehalf}
        %\captionof{figure}{Worst Case complexity}
      }
\null\par\null
      \parbox{\linewidth} {
        \centering
        \includegraphics[scale=0.4]{data_freqhalf}
        %\captionof{figure}{Worst Case complexity}
      }
\null\par\null
      \parbox{\linewidth} {
        \centering
        \includegraphics[scale=0.4]{psd-freqhalfideal_data_out}
        %\captionof{figure}{Worst Case complexity}
      }

      If we zoom in to see the actual frequency components, we see:\\
      \parbox{\linewidth} {
        \centering
        \includegraphics[scale=0.4]{half_ideal_fft}
        %\captionof{figure}{Worst Case complexity}
      }
       Thus, we will make two kinds of filters, and try removing/passing certain frequency components
       and analyse the sense of the resulting time domain signal.
\\

       Making a filter, of lower order, having a pass band bandwidth of 30 points, and single sided transition width of 15 points, we obtain the resulting time domain signal as:\\
      \parbox{\linewidth} {
        \centering
        \includegraphics[scale=0.4]{halfless_ideal_data_out}
        %\captionof{figure}{Worst Case complexity}
      }
    As we increase the order of the filter, or equivalently, we reduce the transition width to one point [thereby
    making it more or less ideal], we get a curve with better rejection:\\
      \parbox{\linewidth} {
        \centering
        \includegraphics[scale=0.4]{halfideal_data_out}
        %\captionof{figure}{Worst Case complexity}
      }
      Thus, it has better rejection, and the sine wave is much more smoother in this case.

      \par\null\par
      \subsection{Analysis}
      Between the two of the time domain sequences, we can see that truncation of the time domain sequence,
      has led to lesser information, and thus, also, the frequency components have got closer to zero. Also, 
      higher the order of the filter, smaller the transition width, and therefore more effective the filtering process.
      \pagebreak
\section{Frequency Sampling Method}
\par\null\par
We utilise the freequency sampling method for designing our filter. The method is outlined below.\\
Considering that the desired frequency domain specifications are real in nature, we have $H_r$ as the real part of the desired specifications. Thus, defining real frequency samples:\\
\[
  G(k+\alpha) = (-1)^k H_r (\frac{2\pi}{M}(k+\alpha))
\]
\\
To account for antisymmetry and symmetry considerations, we include another factor$\beta$. Thus, we have our frequency samples at:\\
\[
  H(k+\alpha) = G(k+\alpha) \exp(j\pi k + j(\beta \frac{\pi}{2} - 2\pi (k+\alpha) \frac{M-1}{2M}))
\]
Thus, evaluating the inverse DFT of which, we can evaluate our desired time domain specifications.

      \pagebreak
\section{Appendix A: Filter Design code}
\lstinputlisting{proc.py}
\pagebreak

\section{Appendix B: FIR Filter Module}
\lstinputlisting{fir.py}

\pagebreak
\end{document}
