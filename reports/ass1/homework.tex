\documentclass{article}

\usepackage{fancyhdr}
\usepackage{extramarks}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{amsfonts}
\usepackage{tikz}
\usepackage{listings}
\usepackage[plain]{algorithm}
\usepackage{algpseudocode}
\usepackage{graphicx}
\usepackage[hidelinks]{hyperref}
\usepackage{xcolor}

% Basic Document Settings
%

\topmargin=-0.45in
\evensidemargin=0in
\oddsidemargin=0in
\textwidth=6.5in
\textheight=9.0in
\headsep=0.25in

\linespread{1.1}

\pagestyle{fancy}
\lhead{\hmwkAuthorName}
\chead{\hmwkClass:\ \hmwkTitle}
\rhead{\firstxmark}
\lfoot{\lastxmark}
\cfoot{\thepage}

\renewcommand\headrulewidth{0.4pt}
\renewcommand\footrulewidth{0.4pt}

\setlength\parindent{0pt}

%
% Create Problem Sections
%

%\newcommand{\enterProblemHeader}[1]{
%    \nobreak\extramarks{}{Problem \arabic{#1} continued on next page\ldots}\nobreak{}
%    \nobreak\extramarks{Problem \arabic{#1} (continued)}{Problem \arabic{#1} continued on next page\ldots}\nobreak{}
%}

%\newcommand{\exitProblemHeader}[1]{
%    \nobreak\extramarks{Problem \arabic{#1} (continued)}{Problem \arabic{#1} continued on next page\ldots}\nobreak{}
%    \stepcounter{#1}
%    \nobreak\extramarks{Problem \arabic{#1}}{}\nobreak{}
%}

\setcounter{secnumdepth}{0}
\newcounter{partCounter}

%
% Homework Problem Environment
%
% This environment takes an optional argument. When given, it will adjust the
% problem counter. This is useful for when the problems given for your
% assignment aren't sequential. See the last 3 problems of this template for an
% example.
%

%
% Homework Details
%   - Title
%   - Due date
%   - Class
%   - Section/Time
%   - Instructor
%   - Author
%

\newcommand{\hmwkTitle}{Assignment\ \#1}
\newcommand{\hmwkDueDate}{Spring 2016}
\newcommand{\hmwkClass}{Real Time Signal Processing Lab}
\newcommand{\hmwkClassInstructor}{Professor Debdoot Sheet}
\newcommand{\hmwkAuthorName}{Anirud Thyagharajan}
\newcommand{\hmwkRollNo}{12EE35011}
\newcommand{\matr}[1]{\mathbf{#1}}
\graphicspath{{images/}}

%
% Title Page
%

\title{
    \vspace{2in}
    \textmd{\textbf{\hmwkClass:\ \hmwkTitle}}\\
    \normalsize\vspace{0.1in}\small{\hmwkDueDate}\\
    \vspace{0.1in}\large{\textit{\hmwkClassInstructor}}
    \vspace{3in}
}

\author{\textbf{\hmwkAuthorName}\\ \textbf{\hmwkRollNo}}
\date{}

\renewcommand{\part}[1]{\textbf{\large Part \Alph{partCounter}}\stepcounter{partCounter}\\}

%
% Various Helper Commands
%

% Useful for algorithms
\newcommand{\alg}[1]{\textsc{\bfseries \footnotesize #1}}

% For derivatives
\newcommand{\deriv}[1]{\frac{\mathrm{d}}{\mathrm{d}x} (#1)}

% For partial derivatives
\newcommand{\pderiv}[2]{\frac{\partial}{\partial #1} (#2)}

% Integral dx
\newcommand{\dx}{\mathrm{d}x}

% Alias for the Solution section header
\newcommand{\solution}{\textbf{\large Solution}}

% Probability commands: Expectation, Variance, Covariance, Bias
\newcommand{\E}{\mathrm{E}}
\newcommand{\Var}{\mathrm{Var}}
\newcommand{\Cov}{\mathrm{Cov}}
\newcommand{\Bias}{\mathrm{Bias}}

\begin{document}

\maketitle

\pagebreak

  \lstset{language=Python,
          basicstyle=\ttfamily,
          keywordstyle=\color{blue}\ttfamily,
          stringstyle=\color{red}\ttfamily,
          commentstyle=\color{green}\ttfamily,
}
\section{Time Complexity of Searching and Sorting Algorithms}
\par\null\par
    The objective of the assignment was to evaluate the worst and average case complexity of the following algorithms:
    \begin{itemize}
      \item Linear Search
      \item Selection Sort
      \item Bubble Sort
      \item Discrete Fourier Transform Algorithm
    \end{itemize}
    We will deal with each of the above algorithms, discuss their pseudo code, and present the worse case and average case complexity. 
    \par\null\par
    The Python files are appended at the end. Apart from this, the respective implementations can be found with their algorithms.
    \par\null\par
    I also use \textbf{Git} for version control, so you could find my repository at \textcolor{blue}{\underline{\href{https://bitbucket.org/anirudt_bit/rtsplab/src}{this link}}}.

\pagebreak

\section{Linear Search}
\par\null\par
Linear search, or sequential search is an algorithm for finding a certain key in an array, which enlists comparison of the value of every element of the array with the key, and returning that element, or exiting if the key is not found till the end of the array. If $n$ being the size of the array, the worst case time complexity turns out to be $O\left( n \right)$. Also, the expected or average time cost also evaluates to $O\left( n \right)$.
\\
This is the Python implementation of Linear Search.

\lstinputlisting[firstline=17,lastline=24]{search.py}

Following are graphs displaying the worst case and average case complexities of linear search.
\\
      \parbox{\linewidth} {
        \centering
        \includegraphics[scale=0.4]{linear}
        %\captionof{figure}{Worst Case complexity}
      }
      \\
      \parbox{\linewidth} {
        \centering
        \includegraphics[scale=0.4]{linear_avg}\\
        %\captionof{figure}{Average case complexity}
      }
\pagebreak
\section{Selection Sort}
\par\null\par
Selection sort is an in-place comparison sort, used for sorting arrays. It has $O\left( n^2 \right) $ time complexity, both average case and worst case, making it inefficient on large lists, though it is noted for its simplicity and has performance advantages over other methods, where auxiliary memory is limited.
\\
This is the Python implementation of Selection sort.
\lstinputlisting[firstline=25, lastline=34]{search.py}
      \parbox{\linewidth} {
        \centering
        \includegraphics[scale=0.4]{select}
        %\captionof{figure}{Worst Case complexity}
      }
      \\
      \parbox{\linewidth} {
        \centering
        \includegraphics[scale=0.4]{select_avg}\\
        %\captionof{figure}{Average case complexity}
      }

\pagebreak
\section{Bubble Sort}
\par\null\par
Bubble sort is a simple sorting algorithm that continuously steps through the array to be sorted, and compares each pair of adjacent array elements and swaps them if they are in the wrong order. Bubble sort also has $O\left( n^2 \right)$ complexity for worst case and average case.

\lstinputlisting[firstline=36, lastline=43]{search.py}
      \parbox{\linewidth} {
        \centering
        \includegraphics[scale=0.4]{bubble}
        %\captionof{figure}{Worst Case complexity}
      }
      \\
      \parbox{\linewidth} {
        \centering
        \includegraphics[scale=0.4]{bubble_avg}\\
        %\captionof{figure}{Average case complexity}
      }

\pagebreak
\section{DFT Algorithm}
\par\null\par
Discrete Fourier Transform (DFT) is an algorithm that converts a finite sequence of equally spaced function samples from the time domain onto a combination of finite coefficients of complex sinusoids, ordered by their frequencies, in the frequency domain. DFT also has a time complexity of $O\left( n^2 \right)$.

\lstinputlisting[firstline=6, lastline=14]{dft.py}
      \parbox{\linewidth} {
        \centering
        \includegraphics[scale=0.4]{dft}
        %\captionof{figure}{Worst Case complexity}
      }
      \parbox{\linewidth} {
        \centering
        \includegraphics[scale=0.4]{dft_exec}
        %\captionof{figure}{Worst Case complexity}
      }
\pagebreak

\section{Appendix A: Search/Sort algorithms code}
The following code depicts the worst and average case complexities of sorting and search algorithms.
\lstinputlisting{search.py}
\pagebreak

\section{Appendix B: DFT Algorithm code}
The following code depicts the complexity of the DFT algorithm.
\lstinputlisting{dft.py}
\pagebreak
\end{document}
