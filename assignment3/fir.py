#!/bin/bash

"""
The following example deals with FIR filter design using frequency sampling method, 
assuming that the impulse response is real in nature. Utilizing this simplification,
we can express the impulse response in an elegant way. The following program 
demonstrates this particular algorithm-hack.
"""

import cmath
import numpy as np
import time
import matplotlib.pyplot as plt
import pdb
import sys

# CONSTANTS DEFINITION
pi = cmath.pi


def dft(x):
    x = np.array(x)
    N = len(x)
    if N%2:
        t = range(-N/2+1, N/2+1)
    else:
        t = range(-N/2, N/2)
    W_N = cmath.exp(-2J*cmath.pi*1.0/N)
    W_N_mat = []
    for i in t:
        tmp = [W_N**(i*j) for j in t]
        W_N_mat.append(tmp)
    W_N_mat = np.array(W_N_mat)
    #pdb.set_trace()
    #print W_N_mat
    X = np.dot(W_N_mat, x)
    #pdb.set_trace()
    return X

def idft(X): 
    X = np.array(X)
    N = len(X)
    if N%2:
        t = range(-N/2+1, N/2+1)
    else:
        t = range(-N/2, N/2)
    W_N = cmath.exp(-2J*cmath.pi*1.0/N)
    W_N_mat = []
    for i in t:
        tmp = [W_N**(i*j) for j in t]
        W_N_mat.append(tmp)
    W_N_mat = np.array(W_N_mat)
    #pdb.set_trace()
    x = np.dot(np.linalg.inv(W_N_mat),X)
    #pdb.set_trace()
    return x

def input_proc(st, key):
    # if antisymmetric
    if st == "anti":
        beta = 1
        alpha = 0
        H_r = []

    if st == "symm" and key == "1":
    # if symmetric
        beta = 0
        alpha = 0
        H_r = np.array([0, 0, 0, 0.4, 1, 1, 1, 1, 1, 1, 1, 0.4, 0, 0, 0])

    if st == "symm" and key == "2":
        beta = 0
        alpha = 0
        T1 = 0.3789795
        H_r = [0, 0, 0, 0, 0, 0, 0, 0, T1, 1, 1, 1, 1, 1, 1]
        H_r = H_r + list(reversed(H_r))
        print "Here it is"
        print H_r

    return alpha, beta, H_r

def graphify(x, y, xlabel, ylabel, title, name):
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title(title)
    plt.stem(x,y)
    plt.grid = True
    plt.savefig(name+'.png')
    plt.show()

def main():
    # Desired H, and beta for displaying symmetry
    alpha, beta, H_r = input_proc("symm", "1")
    M = len(H_r)

    # Calculating H(k)
    H_k = np.array([H_r[i]*cmath.exp(1j*(beta*pi/2 - 2*pi*(i+alpha)*(M-1)/2*M)) \
            for i in range(0, len(H_r))])
    print H_k
    print H_r

    #pdb.set_trace()

    # Calculating impulse response
    h = idft(H_k)
    print h
    t = range(-M/2+1, M/2+1)
    #graphify(t, abs(h), "Time", "Impulse Response", "FIR filter coefficients",\
    #        "fir")

    # Verification step
    H = dft(h)
    print H

if __name__ == '__main__':
    main()
