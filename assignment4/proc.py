import numpy as np
import cmath
import scipy.io
import pdb
import matplotlib.pyplot as plt
from scipy.fftpack import *
import fir

def load_filter(s, N):
    H = []
    if s is "ideal":
        for i in range(N):
            if i > N//2-15 and i<N//2+15:
                H.append(1)
            if i==N//2-15 or i==N//2+15:
                H.append(0.5)
            else:
                H.append(0)
        name = "ideal_data_out"

    if s is "less_ideal":
        print "Entered"
        for i in range(N):
            if i > N//2-15 and i<N//2+15:
                H.append(1)
            if (i > N//2-30 and i < N//2-15):
                H.append((1.0*i - (N//2-15))*1.0/15) 
            if (i > N//2+15 and i < N//2+30):
                H.append((-1.0*i + (N//2+30))*1.0/15)
            else:
                H.append(0)
        print len(H)
        name = "less_ideal_data_out"
    return H, name

def graphify(x, y, xlabel, ylabel, title, name):
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title(title)
    plt.stem(x, y)
    plt.grid = True
    plt.savefig(name+'.png')
    plt.show()

def graphify_plot(x, y, xlabel, ylabel, title, name, axis=None):
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.title(title)
    plt.plot(x, y)
    plt.grid = True
    plt.savefig(name+'.png')
    plt.show()
    if axis != None:
        plt.axis(axis)

def proc(extent,ideality):
    # Loading data, formulate the data
    data = scipy.io.loadmat('mgh005m.mat')
    if extent == "full":
        val = list(np.array(data['val'])[0])
        t = range(-10800, 10800)
    else:
        val = list(np.array(data['val'])[0])
        val = val[0:10000]
        t = range(-5000, 5000)

    N = len(val)

    # Frequency response
    X = np.fft.fftshift(fft(val))
    # pdb.set_trace()

    # Filter trying.
    H, name = load_filter(ideality, len(X))

    graphify_plot(range(len(X)), val, "Time", "Data", "data", "data_time"+extent+name)
    # Assuming sampling frequency is 21600/2 = 10800
    freq = range(len(X))
    psd = abs(X)

    graphify_plot(t, abs(X), "Time", "Data", "data", "data_freq"+extent+name)
    graphify_plot(t, psd, "Freq", "PSDroot", "psd", "psd-freq"+extent+name)

    # We will use the earlier module to
    # construct the practical impulse response
    # filter coefficients.
    H = fir.main([0,0,H])

    # FFT Mult is Time-Dom Conv
    Y = [H[i]*X[i] for i in range(len(X))]
    Y = np.fft.ifftshift(Y)

    # IFFT
    y = ifft(Y)
    graphify_plot(range(len(y)), y, "Time", "Data", "y", extent+name)
    print abs(y)

def main():
    proc("full", "ideal")
    proc("full", "less_ideal")
    proc("half", "ideal")
    proc("half", "less_ideal")


if __name__ == '__main__':
    # graphify_plot(t, abs(X), "Time", "Data", "data", "data_freq", [-50, 25000, min(val), max(val)])
    main()
